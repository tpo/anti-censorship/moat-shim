module gitlab.torproject.org/tpo/anti-censorship/moat-shim

go 1.18

require github.com/fsnotify/fsnotify v1.7.0

require golang.org/x/sys v0.4.0 // indirect
