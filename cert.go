package main

import (
	"crypto/tls"
	"log"
	"sync"

	"github.com/fsnotify/fsnotify"
)

type Cert struct {
	sync.RWMutex

	cert    *tls.Certificate
	watcher *fsnotify.Watcher

	certFile string
	keyFile  string
}

func NewCert(certFile, keyFile string) (*Cert, error) {
	c := &Cert{
		certFile: certFile,
		keyFile:  keyFile,
	}

	err := c.read()
	if err != nil {
		return nil, err
	}

	c.watcher, err = fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}
	if err := c.watcher.Add(certFile); err != nil {
		return nil, err
	}
	if err := c.watcher.Add(keyFile); err != nil {
		return nil, err
	}

	go c.Watch()

	return c, nil
}

func (c *Cert) read() error {
	c.Lock()
	defer c.Unlock()

	cert, err := tls.LoadX509KeyPair(c.certFile, c.keyFile)
	if err != nil {
		return err
	}

	log.Println("New certificate loaded")
	c.cert = &cert
	return nil
}

func (c *Cert) Get(_ *tls.ClientHelloInfo) (*tls.Certificate, error) {
	c.RLock()
	defer c.RUnlock()

	return c.cert, nil
}

func (c *Cert) Watch() {
	for {
		select {
		case event := <-c.watcher.Events:
			if event.Has(fsnotify.Remove) || event.Has(fsnotify.Rename) {
				// keep watching if it gets deleted
				err := c.watcher.Add(event.Name)
				if err != nil {
					log.Println("Error watching file", event.Name, ":", err)
				}
			}
			if event.Has(fsnotify.Create) || event.Has(fsnotify.Write) {
				err := c.read()
				if err != nil {
					log.Println("Error loading cert:", err)
				}
			}

		case err := <-c.watcher.Errors:
			log.Println("Error watching for certificate:", err)
		}
	}
}
