## Info

This is a shim program for Moat to parse out the USERADDR from an ExtORPort connection to the meek server. This was written to pass client addresses to BridgeDB as described in https://gitlab.torproject.org/tpo/anti-censorship/bridgedb/-/issues/32276

Some of this code is lifted directly from https://gitweb.torproject.org/pluggable-transports/goptlib.git because it is the mirror image of what pluggable transports expect from the PT server.

## Deployment

Build and run this server alongside the meek-server on polyanthum.

## Local Testing

These are the steps I took to try this shim out locally before deploying it:

- Run a simple web server somewhere locally on whichever port you like (say, :8080).
   Here is a simple go web server that will print out the request headers so you can check the presence of `X-Forwarded-For`:
    ```
    package main

    import (
        "log"
        "net/http"
    )

    type SimpleHandler struct{}

    func (h *SimpleHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
        data := []byte("Hi, I'm a simple web server\n")

        //Print out headers
        for h, _ := range r.Header {
            log.Printf("%s: %s\n", h, r.Header.Get(h))
        }
        w.Write(data)
    }

    func main() {

        handler := &SimpleHandler{}
        http.ListenAndServe(":8080", handler)

    }

    ```
- Build and run the shim with the `-proxy` argument set to the local port above, and the -addr argument set to whichever port you would like the shim to liston on (say, :5000)
   ```
   ./moat-shim -proxy http://127.0.0.1:8080 -addr :5000 -auth cookie.dat
   ```
- Set up and run a meek server:
   ```
   export TOR_PT_MANAGED_TRANSPORT_VER=1
   export TOR_PT_SERVER_BINDADDR=meek-0.0.0.0:2000
   export TOR_PT_SERVER_TRANSPORTS=meek
   export TOR_PT_EXTENDED_SERVER_PORT=127.0.0.1:5000
   export TOR_PT_AUTH_COOKIE_FILE=cookie.dat

   ./meek-server --disable-tls & disown
   ```
- then run the meek client
   ```
   export TOR_PT_MANAGED_TRANSPORT_VER=1
   export TOR_PT_CLIENT_TRANSPORTS=meek
   ./meek-client -url http://localhost:2000
   ```
   This will print out which port the meek client's socks5 proxy is listening on (for the purpose of this example, let's say it's port 44459).

- Make a curl request to your simple web server through the socks proxy
   ```
   curl --socks5 127.0.0.1:44459 http://localhost:5000
   ```
